const express = require("express");
const router = express.Router();
const { verify, verifyAdmin } = require("../middleware/auth");

//Get Products
const { getProducts } = require("../controllers/productController");
router.get("/", getProducts);
//Get Single Products
const { getSingleProduct } = require("../controllers/productController");
router.get("/:id", getSingleProduct);
//Post Products
const { postProducts } = require("../controllers/productController");
router.post("/", verify, verifyAdmin, postProducts);
//Put Products
const { putProducts } = require("../controllers/productController");
router.put("/:id", verify, verifyAdmin, putProducts);
//PUT - products/archive/:id
const { inactivateProduct } = require("../controllers/productController");
router.put("/archive/:id", verify, verifyAdmin, inactivateProduct);
//PUT - products/activate/:id
const { activateProduct } = require("../controllers/productController");
router.put("/activate/:id", verify, verifyAdmin, activateProduct);

module.exports = router;
