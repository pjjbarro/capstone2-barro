const express = require("express");
const router = express.Router();
const { verify, verifyAdmin } = require("../middleware/auth");

//POST - users
const { createUser } = require("../controllers/userController");
router.post("/", createUser);
//POST - users/login
const { loginUser } = require("../controllers/userController");
router.post("/login", loginUser);
//PUT - users/setAsAdmin/:id
const { setAsAdminUser } = require("../controllers/userController");
router.put("/setAsAdmin/:id", verify, verifyAdmin, setAsAdminUser);

module.exports = router;
