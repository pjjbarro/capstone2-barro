const express = require("express");
const router = express.Router();
const { verify, verifyAdmin } = require("../middleware/auth");
//POST Orders
const { createOrder } = require("../controllers/orderController");
router.post("/", verify, createOrder);
//GET User's Orders
const { getUserOrder } = require("../controllers/orderController");
router.get("/user", verify, getUserOrder);
//GET All Orders
const { getAllOrder } = require("../controllers/orderController");
router.get("/allOrder", verify, verifyAdmin, getAllOrder);

module.exports = router;
