# MotoGarage Store: Prince Joemarie E-commerce API

## Features:

- User registration
- User authentication
- Set user as admin (Admin only)
- Create Product
- Retrieve All Active Products
- Retrieve All Products (Admin only)
- Update Product
- Archive Product (Admin only)
- Search Products
- Get Products By Category
- Create Order
- Update Order Status (Admin only)
- Get authenticated user's orders
- Get All Orders

### User registration :

POST - http://localhost:8000/users
Body (JSON):
{
"firstName": String,
"lastName": String,
"email": String,
"password": String,
"mobileNo": String,
}

### User authentication :

POST - http://localhost:8000/users/login
Body (JSON):
{
"email": String,
"password": String
}

### Set as Admin :

PUT - http://localhost:8000/users/setAsAdmin/616be9750502d5afe28ed3de
Body: No Request Body
Admin Token Required

### Retrieve All Active Products :Retrieve All Active Products :

GET - http://localhost:8000/products

### Create Product :

POST - http://localhost:8000/products
Body (JSON):
{
"name": "Suspensions",
"description": "Fork Accessories,Lowering Links,Shocks,Swingarm Extensions",
"price": "2500"
}

### Retrieve All Active Products :Retrieve All Active Products :

GET - http://localhost:8000/products

### Retrieve Single Product

GET -http://localhost:8000/products/616bbc020cfeb43baa2c8640

### Update Product

PUT - http://localhost:8000/products/616bbc020cfeb43baa2c8640
Body (JSON):
{
"name": "Lights and Electrical",
"description": "Accent Lighting,Batteries and Chargers,Bulbs,Cameras",
"price": "12000"
}
Admin Token Required

### Archive Product :

PUT - http://localhost:8000/products/archive/616bbc020cfeb43baa2c8640
Body (JSON): No request body
Admin Token Required

### Activate Product

PUT - http://localhost:8000/products/activate/616bbc020cfeb43baa2c8640
Body (JSON): No request body
Admin Token Required

### Non-admin User checkout (Create Order) :

POST - http://localhost:8000/orders
{
"totalAmount": 12000,
"userId": "616bc459119efb448ccdedcd",
"products": [{
"name": "Tools",
"quantity": 4,
"price": 4800,
"productID": "616bbc020cfeb43baa2c8640"
}]
}

### Retrieve Authenticated User's orders

GET - http://localhost:8000/orders/user

### Retrieve All Orders

GET - http://localhost:8000/orders/allOrder
Admin Token Required

### Admin Credentials:

email: adminAPI@gmail.com
password: admin456
{
"email": "adminAPI@gmail.com",
"password": "admin456"
}

Live Demo:
https://frozen-basin-81694.herokuapp.com/
