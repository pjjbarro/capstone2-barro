const User = require("../models/User");
const bcrypt = require("bcrypt");
const { createAccessToken } = require("../middleware/auth");

module.exports.createUser = (request, response) => {
  const document = new User({
    firstName: request.body.firstName,
    lastName: request.body.lastName,
    mobileNo: request.body.mobileNo,
    email: request.body.email,
    password: bcrypt.hashSync(request.body.password, 10),
  });
  document
    .save()
    .then((saveDocument) => {
      return response.send(saveDocument);
    })
    .catch((error) => response.send(error));
};
module.exports.loginUser = (request, response) => {
  User.findOne({ email: request.body.email })
    .then((results) => {
      //1 . check if user exists
      if (results == null) {
        return response.send({ message: "User Not Found!" });
      }
      // 2. check if password is correct
      const isPasswordCorrect = bcrypt.compareSync(
        request.body.password,
        results.password
      );
      if (isPasswordCorrect == true) {
        return response.send({ token: createAccessToken(results) });
      } else {
        return response.send({ message: "Password Incorrect!" });
      }
    })
    .catch((error) => response.send(error.message));
};
module.exports.setAsAdminUser = (request, response) => {
  User.findByIdAndUpdate(request.params.id, { isAdmin: true }, { new: true })
    .then((saveDocument) => {
      return response.send(saveDocument);
    })
    .catch((error) => response.send(error));
};
