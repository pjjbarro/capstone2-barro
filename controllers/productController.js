const Product = require("../models/Product");

module.exports.getProducts = (request, response) => {
  Product.find({})
    .then((results) => response.send(results))
    .catch((error) => response.send(error));
};
module.exports.getSingleProduct = (request, response) => {
  Product.findById(request.params.id)
    .then((results) => response.send(results))
    .catch((error) => response.send(error));
};
module.exports.postProducts = (request, response) => {
  const document = new Product({
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
  });
  document
    .save()
    .then((saveDocument) => {
      return response.send(saveDocument);
    })
    .catch((error) => response.send(error));
};
module.exports.putProducts = (request, response) => {
  let updates = {
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
  };
  Product.findByIdAndUpdate(request.params.id, updates, { new: true })
    .then((saveDocument) => {
      return response.send(saveDocument);
    })
    .catch((error) => response.send(error));
};
module.exports.inactivateProduct = (request, response) => {
  let updates = {
    isActive: false,
  };
  Product.findByIdAndUpdate(request.params.id, updates, { new: true })
    .then((saveDocument) => {
      return response.send(saveDocument);
    })
    .catch((error) => response.send(error));
};
module.exports.activateProduct = (request, response) => {
  let updates = {
    isActive: true,
  };
  Product.findByIdAndUpdate(request.params.id, updates, { new: true })
    .then((saveDocument) => {
      return response.send(saveDocument);
    })
    .catch((error) => response.send(error));
};
