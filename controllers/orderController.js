const Order = require("../models/Order");

module.exports.createOrder = (request, response) => {
  const document = new Order({
    totalAmount: request.body.totalAmount,
    userId: request.user.id,
    products: request.body.products,
  });
  document
    .save()
    .then((saveDocument) => {
      return response.send(saveDocument);
    })
    .catch((error) => response.send(error));
};

module.exports.getUserOrder = (request, response) => {
  Order.find({ userId: request.user.id })
    .then((results) => response.send(results))
    .catch((error) => response.send(error));
};
module.exports.getAllOrder = (request, response) => {
  Order.find({})
    .then((results) => response.send(results))
    .catch((error) => response.send(error));
};
