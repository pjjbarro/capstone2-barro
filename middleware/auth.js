/*We will create a module which will have functions and methods that will help us authenticate our users to either give them permission or restrict them from an action. To do this, first we need to give our users a key to access our app.*/

const jwt = require("jsonwebtoken");
const secret = "JSHoemsh";

module.exports.createAccessToken = (user) => {
  //console.log(user);
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  //create your jwt with the payload, with the secret, the algorithm to create your JWT
  return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
  //Token passed as bearer tokens can be found in which part of your request?
  /*console.log(req.headers.authorization)*/
  //Token variable will hold our beaer token from our client.
  let token = req.headers.authorization;
  /*console.log(token);*/
  if (typeof token === "undefined") {
    return res.send({ auth: "Failed. No Token" });
  } else {
    //extract the token and remove the "Bearer" from our token variable.
    token = token.slice(7, token.length);
    //console.log(token);

    //verify the legitimacy of your token:
    jwt.verify(token, secret, function (err, decodedToken) {
      //err will contain the error from our decoding our token.
      //decodedToken is our token after completing and accomplishing verification of its legitimacy against our secret.
      if (err) {
        return res.send({
          auth: "Failed",
          message: err.message,
        });
      } else {
        console.log(decodedToken); //contains the data payload from our token
        //We will add a new property in the req object called user. Assign to be decoded data to that property
        req.user = decodedToken;
        //next() will allow us to run the next function. (another Middleware or the controller)
        next();
      }
    });
  }
};

module.exports.verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: "Failed",
      message: "Action Forbidden",
    });
  }
};
