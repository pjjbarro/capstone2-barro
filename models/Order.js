const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
  totalAmount: { type: Number, required: [true, "Total Amount is Required! "] },
  purchaseOn: { type: Date, default: new Date() },
  userId: { type: String, required: [true, "User ID is Required! "] },
  products: [
    {
      name: { type: String, required: [true, "Product Name is Required! "] },
      quantity: {
        type: Number,
        required: [true, "Quantity number is Required! "],
      },
      price: { type: Number, required: [true, "Price is required "] },
      productID: { type: String, required: [true, "ProductId is Required! "] },
    },
  ],
});

module.exports = mongoose.model("Order", orderSchema);
