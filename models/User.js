const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  firstName: { type: String, required: [true, "FirstName is Required! "] },
  lastName: { type: String, required: [true, "LastName is Required! "] },
  mobileNo: { type: String, required: [true, "mobileNo is required! "] },
  email: { type: String, required: [true, "Email is Required! "] },
  password: { type: String, required: [true, "Password is Required! "] },
  isAdmin: { type: Boolean, default: false },
});

module.exports = mongoose.model("User", userSchema);
