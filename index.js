const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
//database config+
mongoose.connect(
  "mongodb+srv://Phyro10:Teambahay123@cluster0.iuuue.mongodb.net/MotoGarage?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);

const db = mongoose.connection;
db.on(
  "error",
  console.error.bind(console, "There is an error connecting to mongoDB")
);
db.once("open", () => console.log("Your successfully connected!"));
//Express config
const app = express();
const port = process.env.PORT || 8000;
app.use(cors());
app.use(express.json());
//express routes
const orderRoutes = require("./routes/orderRoutes");
app.use("/orders", orderRoutes);
const productRoutes = require("./routes/productRoutes");
app.use("/products", productRoutes);
const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);
app.listen(port, () => console.log("My app is runing" + port));
